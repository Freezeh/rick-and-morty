const path = require('path');

module.exports = {
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/*.config.js',
    '!**/*.test.{js,jsx}',
    '!./tests/**',
  ],
  coverageThreshold: {
    global: {
      statements: 100,
      branches: 100,
      lines: 100,
      functions: 100,
    },
  },
  clearMocks: true,
  verbose: false,
  watchPathIgnorePatterns: ['/node_modules/'],
  roots: ['./'],
  coverageProvider: 'v8',
  coverageReporters: ['html', 'text', 'json'],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/.vscode/',
    '/.next/',
    '/.git/',
    '/coverage/',
  ],
  testRegex: '\\.test\\.jsx?$',
  setupFilesAfterEnv: [
    '@testing-library/jest-dom/extend-expect',
    path.resolve(__dirname, 'tests', 'setup', 'test-setup.js'),
  ],
  testEnvironment: 'jsdom',
};
