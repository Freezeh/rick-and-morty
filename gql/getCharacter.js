import gql from 'graphql-tag';

export const GET_CHARACTER = gql`
  query getCharacter($characterId: ID!) {
    character(id: $characterId) {
      name
      status
      species
      gender
      image
      created
    }
  }
`;