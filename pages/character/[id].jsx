import React from 'react';
import { useRouter } from 'next/router'
import { withApollo } from '../../libs/apollo';
import { useQuery } from '@apollo/react-hooks';
import { GET_CHARACTER } from '../../gql/getCharacter';
import Head from 'next/head'
import Profile from '../../components/Profile'

const Character = () => {
  const router = useRouter()
  const { id } = router.query

  const { loading, error, data } = useQuery(GET_CHARACTER, {
    variables: {
      characterId: id
    }
  });
  if (error) {
    console.error(error)
    return <h1>Error</h1>;  
  }

  return <>
    {loading
      ? <p>Loading...</p> :
      <>
      <Head>
        <title>⚗️ Rick & Morty Characters | {data.character.name}</title>
        <meta name="description" content={`Name: ${data.character.name} | Species: ${data.character.species}`} />
      </Head>
      <Profile character={data.character} />
      </>
    }
  </>
}

export default withApollo({ssr: true})(Character)