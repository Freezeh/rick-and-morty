import React from 'react'
import GlobalReset from '../components/GlobalReset'
import GlobalNavigation from '../components/GlobalNavigation'

const App = ({Component, pageProps}) => {
  return  <>
    <GlobalReset/>
    <GlobalNavigation/>
    <Component {...pageProps} />
  </>
}

export default App