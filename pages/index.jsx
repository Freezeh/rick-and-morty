import React from 'react';
import { withApollo } from '../libs/apollo';
import { useQuery } from '@apollo/react-hooks';
import { ALL_CHARACTERS } from '../gql/allCharacters';
import Link from 'next/link'
import Head from 'next/head'

const IndexPage = () => {
    const { loading, error, data } = useQuery(ALL_CHARACTERS);
    if (error) return <h1>Error</h1>;

    return (
        <>
        <Head>
            <title>⚗️ Rick & Morty Characters</title>
            <meta name="description" content="Information about all your favourite Rick & Morty characters" />
        </Head>
            <h1>All Characters</h1>
            {loading 
            ? <p>Loading...</p> 
            : <div>
                {data.characters.results.map((data) => (
                    <ul key={data.id}>
                        <li>
                            <Link href={`/character/${data.id}`}>
                                <a>{data.id} {data.name}</a>
                            </Link>
                        </li>
                    </ul>
                ))}
            </div>
            }
        </>
    );
};

export default withApollo({ ssr: true })(IndexPage);