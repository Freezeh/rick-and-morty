import React from 'react';
import Homepage from '../../pages/index';
import { render, waitFor } from '../utils';

test('renders the homepage', async () => {
  const { getByText, queryByText } = render(<Homepage />);

  expect(getByText('Loading...')).toBeInTheDocument();
  await waitFor(() => expect(queryByText('1 Rick Sanchez')).toBeInTheDocument());
});
