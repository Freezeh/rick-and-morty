import { cleanup } from '@testing-library/react';

afterEach(() => {
  if (typeof window !== 'undefined') {
    cleanup();
  }
});

afterAll(() => {});
