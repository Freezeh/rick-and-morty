import React from 'react';
import { render } from '@testing-library/react';
import Wrapper from './test-wrapper';

const renderWithoutData = (initialNode, options) => {
  const toRender = (node) => <Wrapper>{node}</Wrapper>;

  return render(toRender(initialNode), options);
};

export * from '@testing-library/react';
export {
  renderWithoutData as render,
};
