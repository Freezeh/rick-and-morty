import React from 'react'
import styled from 'styled-components'
import Link from 'next/link'

const Header = styled.header`
  padding: 8px 16px;
  width: 100%;
  height: 64px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;  
  align-items: center;
  background-color: #e3e3e3;
  border-bottom: 1px solid #333;

  @media (min-width: 768px) {
    align-items: start;
    
  }
`

const Logo = styled.img`
    flex-basis: 25%;
    height: 100%;
    max-width: 25%;
    min-width: 200px;
    cursor: pointer;
`

const GlobalNavigation = () => {

  return <Header>
    <Link href="/">
        <Logo src="/logo.png" alt="Rick and Morty" />
    </Link>
  </Header>
}

export default GlobalNavigation