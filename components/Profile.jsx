import React from 'react'
import CharacterDetails from './CharacterDetails'
import styled from 'styled-components'

const ProfileWrapper = styled.div`
  display: flex;
  margin: 16px auto ;
  padding: 8px;
  border: 1px solid #e3e3e3;
  border-radius: 8px;
  flex-direction: column;
  width: fit-content;

  @media (min-width: 768px) {
    flex-direction: row;
  }
`

const CharacterPortrait = styled.img`
  border-radius: 8px;
`

const Profile = ({character}) => {

  return <ProfileWrapper>
    <CharacterPortrait src={character.image} alt={character.name}/>
    <CharacterDetails character={character}/>
  </ProfileWrapper>
}

export default Profile