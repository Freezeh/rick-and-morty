import React from 'react'
import styled from 'styled-components'
import { format } from 'date-fns'

const DetailsWrapper = styled.div`
  margin-left: 8px;
  font-family: monospace;
  
  dl{
    display: flex;
    flex-flow: row;
    flex-wrap: wrap;
    overflow: visible;
    max-width: fit-content;
  }

  dt {
    flex: 0 0 50%;
    text-overflow: ellipsis;
    overflow: hidden;
    font-style: italic;
  }

  dd {
    flex:0 0 50%;
    margin-left: auto;
    text-align: left;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`

const CharacterDetails = ({character}) => {
  return <DetailsWrapper>
    <h1>{character.name}</h1>
    <dl>
        <dt>Status:</dt><dd>{character.status || 'unknown'}</dd>
        <dt>Species:</dt><dd>{character.species || 'unknown'}</dd>
        <dt>Gender:</dt><dd>{character.gender || 'unknown'}</dd>
        <dt>Created:</dt><dd>{character.created ? format(new Date(character.created), 'dd/MM/yyyy') : 'unknown'}</dd>
    </dl>
  </DetailsWrapper>
}

export default CharacterDetails