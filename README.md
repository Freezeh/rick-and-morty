# ⚛️ React Take Home

A take home test for prospective candidates for Players' Lounge.

## 📓 Breif

Hey there y'all! This peice of work just to check technical proficency. We at Players' Lounge are looking for the process by which you conduct your work in this task and we don't expect you to be able to finish everything.

Please treat this like you would any other task your current/former employer would give you and complete the tasks below in order.

Please dont spend more than an hour or two on this, it should be more than ample time to demonstrate your process to us in your output.

## 🌎🌍🌏 Overview of what is here

This project's job is to give a list of Rick and Morty characters to who ever visits the sites homepage `/`. However, our users don't want to stop there and in reality want to keep learning about their favourite characters and the worlds that they inhabit.

During the course of this task we will be using our knowledge of NextJS and GraphQL to expand our site to give our users a better experience. We will also be putting in place things that ensure our new site works for years to come.

## 🎉 Lets get started

### 💻 Task One - graphql

We have an awesome homepage currently that lists all our favourite characters but its not enough information for our user.

Create a new route that will allow our users to to see more detail about the characters they select.

#### Acceptance Criteria

* Fix the current test for checking `Rick Sanchez` appears on the page.
* A user can click on a characters name on `/` which navigates them to `/character/[id]`
  * e.g. Clicking `Rick Sanchez` navigates the user to `/character/1`
  * `/character/[id]` will then show the user a more detailed breakdown of the characters profile.

### 🎨 Task Two - styles

Great! Our users have a load more content that will give them hours of joy reading through, but it all looks ratehr bland doesn't it?

Also, how would our users get back to the homepage?

Well we already have `styled-components` installed, so why don't we make use of it to construct a Global Navbar and make our site look prettier.

#### Acceptance Criteria

* Create a global CSS reset using `styled-components`.
* Create a `GlobalNavigation` component in `/compoents`, that will link users back to the homepage when licking on a `Rick and Morty` logo.
* Create some general components in `/compoents` to make the `/` and `/character/[id]` pages look nicer.
* Put the `GlobalNavigation` components on ALL pages of our app, both for page that do exist and pages that will exist in the future.

### 🧪 Task Three - testing

This project was started by one of our junior developers from an online tutorial and as such nothing was tested. They started to think about writing tests and how they would acheive that but they need your help to finish up, you have seen there attempt at writting a `Rick Sanchez` test, following that flavour of testing complete the below AC's.

#### Acceptance Criteria

* Add tests around our `/libs` folder, with perticular note to the `withApollo` HOC.
* Create a user journy test to validate that a user can visit the homepage `/` and can navigate to a character `/character/[id]` and then get back to the homescreen.

### 🤖 Task Four - page metadata

A user doesn't want to see localhost:3000 in their tab titles, and this site would be no good for SEO too because we have no metadata set.

#### Acceptance Criteria

* Set an appropriate title on pages we currently have, with a default on every page.
* Set an appropriate description on pages we currently have, with a default on every page.
